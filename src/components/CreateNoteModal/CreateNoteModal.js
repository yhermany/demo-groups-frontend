import Modal from '@material-ui/core/Modal';
import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import validate from 'validate.js';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import ActivityIndicatorWithMessage from '../ActivityIndicatorWithMessage';
import validationRules from './validation-rules';
import EntityApiFacade from '../../core/facades/EntityApiFacade';

export default class CreateNoteModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: this.fieldsForm(),
      formErrors: this.fieldsErrorForm(),
      loading: false,
      error: null,
    };
  }

  default = () => {
    this.setState({
      form: this.fieldsForm(),
      formErrors: this.fieldsErrorForm(),
      loading: false,
      error: null,
    });
  };

  fieldsForm = () => ({
    title: '',
    detail: '',
  });

  fieldsErrorForm = () => ({
    title: null,
    detail: null,
  });

  clearError = () => {
    this.setState({
      formErrors: this.fieldsErrorForm(),
    });
  };

  setField = (event) => {
    const { form } = this.state;
    this.setState({
      form: {
        ...form,
        [event.target.name]: event.target.value,
      },
    });
  };

  setFieldErrors = (errors) => {
    this.setState({
      formErrors: {
        ...errors,
      },
    });
  };

  submit = async (e) => {
    e.preventDefault();
    this.clearError();
    const { form } = this.state;
    const {
      entity, token, onClose, groupId,
    } = this.props;
    const validationResult = validate(form, validationRules);
    if (validationResult) {
      const errors = {};
      Object.entries(validationResult).map(
        ([key, value]) => {
          errors[key] = value.join(' ');
        },
      );
      this.setFieldErrors(errors);
    } else {
      const apiFacade = new EntityApiFacade(token);
      this.setState({ loading: true });
      await apiFacade.createNote({
        groupId,
        note: form,
      }, () => {
        this.default();
        entity.getUserGroup(groupId);
        onClose();
      }, (error) => {
        this.setState({
          error: error.message,
          loading: false,
        });
      });
    }
  };

  render() {
    const { classes, openModal, onClose } = this.props;
    const {
      formErrors, form, loading, error,
    } = this.state;
    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={openModal}
        onClose={() => {
          this.default();
          onClose();
        }}
        className={classes.modal}
      >
        {loading ? <ActivityIndicatorWithMessage message="Cargando ..." /> : (
          <div className={classes.paper}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                <h2>Crear Nota</h2>
              </Grid>
            </Grid>
            <form className={classes.form} noValidate onSubmit={this.submit}>
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="title"
                label="Titulo"
                name="title"
                autoComplete="title"
                autoFocus
                error={!!formErrors.title}
                helperText={formErrors.title}
                value={form.title}
                onChange={this.setField}
              />
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="detail"
                label="Detalle"
                name="detail"
                autoComplete="detail"
                autoFocus
                error={!!formErrors.detail}
                helperText={formErrors.detail}
                value={form.detail}
                onChange={this.setField}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Guardar
              </Button>
            </form>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                {error ? (
                  <Alert severity="error">
                    <AlertTitle>Error</AlertTitle>
                    {error}
                  </Alert>
                ) : null}
              </Grid>
            </Grid>
          </div>
        )}
      </Modal>
    );
  }
}
