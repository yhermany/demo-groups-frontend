const validationRules = {
  title: {
    presence: {
      message: '^Este campo no puede estar vacío.',
      allowEmpty: false,
    },
    length: {
      maximum: 150,
      message: '^Longitud máxima de 150 caracteres.',
    },
  },
  detail: {
    presence: {
      message: '^Este campo no puede estar vacío.',
      allowEmpty: false,
    },
    length: {
      maximum: 200,
      message: '^Longitud máxima de 200 caracteres.',
    },
  },
};

export default validationRules;
