const validationRules = {
  user_id: {
    presence: {
      message: '^Este campo no puede estar vacío.',
      allowEmpty: false,
    },
  },
};

export default validationRules;
