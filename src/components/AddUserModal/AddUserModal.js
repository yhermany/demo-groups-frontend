import Modal from '@material-ui/core/Modal';
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import validate from 'validate.js';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import EntityApiFacade from '../../core/facades/EntityApiFacade';
import validationRules from './validation-rules';
import ActivityIndicatorWithMessage from '../ActivityIndicatorWithMessage';

export default class AddUserModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: this.fieldsForm(),
      formErrors: this.fieldsErrorForm(),
      loading: false,
      error: null,
      users: [],
    };
  }

  componentDidMount() {
    const { token } = this.props;
    const apiFacade = new EntityApiFacade(token);
    apiFacade.getUsers((users) => {
      this.setState({ users });
    });
  }

  default = () => {
    this.setState({
      form: this.fieldsForm(),
      formErrors: this.fieldsErrorForm(),
      loading: false,
      error: null,
    });
  };

  fieldsForm = () => ({
    user_id: 0,
  });

  fieldsErrorForm = () => ({
    user_id: null,
  });

  clearError = () => {
    this.setState({
      formErrors: this.fieldsErrorForm(),
    });
  };

  setField = (event) => {
    const { form } = this.state;
    this.setState({
      form: {
        ...form,
        [event.target.name]: event.target.value,
      },
    });
  };

  setFieldErrors = (errors) => {
    this.setState({
      formErrors: {
        ...errors,
      },
    });
  };

  submit = async (e) => {
    e.preventDefault();
    this.clearError();
    const { form } = this.state;
    const {
      token, onClose, groupId,
    } = this.props;
    const validationResult = validate(form, validationRules);
    if (validationResult) {
      const errors = {};
      Object.entries(validationResult).map(
        ([key, value]) => {
          errors[key] = value.join(' ');
        },
      );
      this.setFieldErrors(errors);
    } else {
      const apiFacade = new EntityApiFacade(token);
      this.setState({ loading: true });
      await apiFacade.addUserGroup({
        groupId,
        form,
      }, () => {
        this.default();
        onClose();
      }, (error) => {
        this.setState({
          error: error.message,
          loading: false,
        });
      });
    }
  };

  render() {
    const { classes, openModal, onClose } = this.props;
    const {
      formErrors, form, loading, error, users,
    } = this.state;
    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={openModal}
        onClose={() => {
          this.default();
          onClose();
        }}
        className={classes.modal}
      >
        {loading ? <ActivityIndicatorWithMessage message="Cargando ..." /> : (
          <div className={classes.paper}>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                <h2>Crear Grupo</h2>
              </Grid>
            </Grid>
            <form className={classes.form} noValidate onSubmit={this.submit}>
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Usuario</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={form.user_id}
                  name="user_id"
                  onChange={this.setField}
                >
                  {users.map((user) => (
                    <MenuItem
                      key={user.id.toString()}
                      value={user.id}
                    >
                      {user.email}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
              >
                <Grid item>
                  {formErrors.user_id ? (
                    <Alert severity="error">
                      {formErrors.user_id}
                    </Alert>
                  ) : null}
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Registrar
              </Button>
            </form>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                {error ? (
                  <Alert severity="error">
                    <AlertTitle>Error</AlertTitle>
                    {error}
                  </Alert>
                ) : null}
              </Grid>
            </Grid>
          </div>
        )}
      </Modal>
    );
  }
}
