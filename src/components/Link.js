import { Link as RouterLink } from 'react-router-dom';
import LinkMUI from '@material-ui/core/Link';
import React from 'react';

export default function Link({ children, ...otherProps }) {
  return (
    <LinkMUI component={RouterLink} {...otherProps}>{children}</LinkMUI>
  );
}
