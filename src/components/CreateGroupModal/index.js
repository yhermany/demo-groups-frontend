import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CreateGroupModal from './CreateGroupModal';
import styles from './styles';
import EntityActionCreator from '../../stores/actions/entity';

const mapStateToProps = (state) => ({
  token: state.auth.token,
  user: state.auth.user,
});

const mapDispatchToProps = (dispatch) => ({
  entity: bindActionCreators(EntityActionCreator, dispatch),
});

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateGroupModal));
