import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import styles from './styles';


const ActivityIndicatorWithMessage = ({ message, ...otherProps }) => (
  <div style={styles.container} {...otherProps}>
    <div>
      <CircularProgress />
    </div>
    <p style={styles.message}>{message}</p>
  </div>
);

export default ActivityIndicatorWithMessage;
