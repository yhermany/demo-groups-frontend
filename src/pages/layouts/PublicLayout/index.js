import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#091D4A',
    },
  },
});

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="h ttps://material-ui.com/">
          German
      </Link>
      {' '}
      {new Date().getFullYear()}
        .
    </Typography>
  );
}

export default function PublicLayout({ children }) {
  return (
    <>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <div>{children}</div>
        <Copyright />
      </MuiThemeProvider>
    </>
  );
}

PublicLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
