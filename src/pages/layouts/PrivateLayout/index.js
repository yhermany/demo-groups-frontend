import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Navbar from '../sections/NavBar';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
          German
      </Link>
      {' '}
      {new Date().getFullYear()}
        .
    </Typography>
  );
}

export default function PrivateLayout({ children }) {
  return (
    <>
      <Navbar />
      <div>{children}</div>
      <Copyright />
    </>
  );
}

PrivateLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
