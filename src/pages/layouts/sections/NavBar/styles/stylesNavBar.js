import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  iconButtonLeft: {
    display: 'none',
    [theme.breakpoints.down('xs')]: {
      display: 'block',
      flexGrow: 1,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  logoImage: {
    flexGrow: 1,
    [theme.breakpoints.only('xs')]: {
      flexGrow: 0,
    },
  },
  buttonsRigth: {
    display: 'block',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  item: {
    flexShrink: 1,
    margin: theme.spacing(2),
    color: 'inherit',
    [theme.breakpoints.down('xs')]: {
      marginTop: theme.spacing(3),
    },
  },
}));
export default useStyles;
