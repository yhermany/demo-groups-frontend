import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { Box } from '@material-ui/core';
import useStyles from './styles/stylesNavBar';
import LabelsOptions from './labels_options';
import Link from '../../../../components/Link';

const NavBar = () => {
  const styles = useStyles();
  const [state, setState] = React.useState({
    open: false,
  });
  const toggleDrawer = (open) => () => {
    setState({ ...state, open });
  };

  return (
    <Box className={styles.root}>
      <AppBar position="static">
        <Toolbar>
          <div className={styles.iconButtonLeft}>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer(true)}
            >
              <MenuIcon />
            </IconButton>
          </div>
          <Box className={styles.logoImage}>
            <Link to="/" className={styles.item}>
              Inicio
            </Link>
          </Box>
          <Box className={styles.buttonsRigth}>
            {LabelsOptions()}
          </Box>
        </Toolbar>
      </AppBar>
      <SwipeableDrawer
        open={state.open}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
        onClick={toggleDrawer(false)}
      >
        {LabelsOptions()}
      </SwipeableDrawer>
    </Box>
  );
};

export default NavBar;
