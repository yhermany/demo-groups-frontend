import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Link from '../../../../components/Link';
import StoreService from '../../../../stores/utils/store';

const useStyles = makeStyles((theme) => ({
  item: {
    flexShrink: 1,
    margin: theme.spacing(2),
    color: 'inherit',
    [theme.breakpoints.down('xs')]: {
      marginTop: theme.spacing(3),
    },
  },
  items: {
    display: 'flex',
    flexDirection: 'row',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
      width: '10em',
      margin: theme.spacing(3),
    },
  },
}));

export default function LabelsOption() {
  const classes = useStyles();
  return (
    <div className={classes.items}>
      <Link
        onClick={(event) => {
          StoreService.logout();
        }}
        to="/"
        className={classes.item}
      >
Salir
      </Link>
    </div>
  );
}
