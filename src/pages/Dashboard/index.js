import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DashBoard from './DashBoard';
import styles from './styles';
import AuthActionCreator from '../../stores/actions/auth';
import EntityActionCreator from '../../stores/actions/entity';

const mapStateToProps = (state) => ({
  user: state.auth.user,
  token: state.auth.token,
  userGroups: state.entity.userGroups,
});

const mapDispatchToProps = (dispatch) => ({
  auth: bindActionCreators(AuthActionCreator, dispatch),
  entity: bindActionCreators(EntityActionCreator, dispatch),
});

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashBoard));
