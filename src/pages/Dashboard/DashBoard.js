import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import CreateGroupModal from '../../components/CreateGroupModal';
import Link from '../../components/Link';
import EntityApiFacade from '../../core/facades/EntityApiFacade';

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
    };
  }

  componentDidMount() {
    const { entity } = this.props;
    entity.getUserGroups();
  }

    activate = (id) => {
      const { token, entity } = this.props;
      const apiFacade = new EntityApiFacade(token);
      apiFacade.activateUserGroup({ groupId: id }, () => {
        entity.getUserGroups();
      });
    };

    render() {
      const { classes, userGroups } = this.props;
      const { openModal } = this.state;
      return (
        <>
          <CreateGroupModal
            onClose={
                        () => {
                          this.setState({ openModal: false });
                        }
                    }
            openModal={openModal}
          />
          <Grid
            container
            spacing={3}
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item xs={12}>
              <h2>Mis Grupos</h2>
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={12}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={() => {
                  this.setState({ openModal: true });
                }}
              >
                            Crear Grupo
              </Button>
            </Grid>
          </Grid>
          <br />
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Nombre</TableCell>
                  <TableCell align="right">Estado</TableCell>
                  <TableCell align="right">Acciones</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {userGroups.map((row) => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {row.group.name}
                    </TableCell>
                    <TableCell align="right">{row.is_active ? 'Activo' : 'No Activo'}</TableCell>
                    <TableCell align="right">
                      <Link to={`/user_group/${row.id}`}> Entrar </Link>
                      <Button
                        onClick={() => {
                          this.activate(row.id);
                        }}
                        color="primary"
                      >
                        {row.is_active ? 'Activar' : 'Desactivar'}
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      );
    }
}
