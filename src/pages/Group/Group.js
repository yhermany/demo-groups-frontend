import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import CreateNoteModal from '../../components/CreateNoteModal';
import AddUserModal from '../../components/AddUserModal';

export default class Group extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      openModalAddUser: false,
    };
  }

  componentDidMount() {
    const { entity, match } = this.props;
    entity.getUserGroup(match.params.id);
  }

  render() {
    const {
      classes, userGroup, user, notes, match,
    } = this.props;
    const { openModal, openModalAddUser } = this.state;
    return (
      <>
        {userGroup ? (
          <>
            <CreateNoteModal
              onClose={
                  () => {
                    this.setState({ openModal: false });
                  }
                }
              groupId={match.params.id}
              openModal={openModal}
            />
            <AddUserModal
              onClose={
                  () => {
                    this.setState({ openModalAddUser: false });
                  }
                }
              groupId={match.params.id}
              openModal={openModalAddUser}
            />
            <Grid
              container
              spacing={3}
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={12}>
                <h2>{`Grupo: ${userGroup.group.name}`}</h2>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item xs={12}>
                {userGroup.is_active
                  ? (
                    <Button
                      variant="contained"
                      color="primary"
                      size="large"
                      onClick={() => {
                        this.setState({ openModal: true });
                      }}
                    >
                                    Crear Nota
                    </Button>
                  ) : null }
                {
                  user.id === userGroup.group.creator.id ? (
                    <Button
                      variant="contained"
                      color="primary"
                      size="large"
                      onClick={() => {
                        this.setState({ openModalAddUser: true });
                      }}
                    >
                    Agregar Usuario
                    </Button>
                  ) : null
                }
              </Grid>
            </Grid>
            <br />
            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Usuario</TableCell>
                    <TableCell align="right">Titulo</TableCell>
                    <TableCell align="right">Detalle</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {notes.map((row) => (
                    <TableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.user.email}
                      </TableCell>
                      <TableCell align="right">{row.title}</TableCell>
                      <TableCell align="right">{row.detail}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </>
        ) : null}
      </>
    );
  }
}
