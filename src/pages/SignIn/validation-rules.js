const validationRules = {
  email: {
    presence: {
      message: '^Este campo no puede estar vacío.',
      allowEmpty: false,
    },
    length: {
      maximum: 150,
      message: '^Longitud máxima de 150 caracteres.',
    },
    email: {
      message: 'no parece un correo electrónico válido',
    },
  },
  password: {
    presence: {
      message: '^Este campo no puede estar vacío.',
      allowEmpty: false,
    },
    length: {
      maximum: 150,
      message: '^Longitud máxima de 150 caracteres.',
    },
  },
};

export default validationRules;
