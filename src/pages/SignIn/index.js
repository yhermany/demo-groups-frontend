import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SignIn from './SignIn';
import styles from './styles';
import AuthActionCreator from '../../stores/actions/auth';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  auth: bindActionCreators(AuthActionCreator, dispatch),
});

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignIn));
