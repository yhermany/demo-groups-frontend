import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SignUp from './SignUp';
import styles from './styles';
import AuthActionCreator from '../../stores/actions/auth';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  auth: bindActionCreators(AuthActionCreator, dispatch),
});

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUp));
