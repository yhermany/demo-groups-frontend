import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import validate from 'validate.js';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import validationRules from './validation-rules';
import Link from '../../components/Link';
import AuthApiFacade from '../../core/facades/AuthApiFacade';
import ActivityIndicatorWithMessage from '../../components/ActivityIndicatorWithMessage';

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: this.fieldsForm(),
      formErrors: this.fieldsErrorForm(),
      loading: false,
      error: null,
    };
  }

    fieldsForm = () => ({
      email: '',
      password: '',
    });

    fieldsErrorForm = () => ({
      email: null,
      password: null,
    });

    clearError = () => {
      this.setState({
        formErrors: this.fieldsErrorForm(),
      });
    };

    setField = (event) => {
      const { form } = this.state;
      this.setState({
        form: {
          ...form,
          [event.target.name]: event.target.value,
        },
      });
    };

    setFieldErrors = (errors) => {
      this.setState({
        formErrors: {
          ...errors,
        },
      });
    };

    submit = async (e) => {
      e.preventDefault();
      this.clearError();
      const { form } = this.state;
      const { auth } = this.props;
      const validationResult = validate(form, validationRules);
      if (validationResult) {
        const errors = {};
        Object.entries(validationResult).map(
          ([key, value]) => {
            errors[key] = value.join(' ');
          },
        );
        this.setFieldErrors(errors);
      } else {
        const authApiFacade = new AuthApiFacade();
        this.setState({ loading: true });
        await authApiFacade.signUp(this.state.form, (response) => {
          this.setState({
            loading: false,
          });
          auth.signIn(response.user, response.token);
        }, (error) => {
          this.setState({
            error: error.message,
            loading: false,
          });
        });
      }
    };

    render() {
      const { classes } = this.props;
      const {
        form, formErrors, loading, error,
      } = this.state;
      return (
        <>
          {loading ? <ActivityIndicatorWithMessage message="Cargando ..." /> : (
            <Container component="main" maxWidth="xs">
              <CssBaseline />
              <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                  <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                                Registrarse
                </Typography>
                <form className={classes.form} noValidate onSubmit={this.submit}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="email"
                    label="Correo Electronico"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    error={!!formErrors.email}
                    helperText={formErrors.email}
                    value={form.email}
                    onChange={this.setField}
                  />
                  <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    name="password"
                    label="Contraseña"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    error={!!formErrors.password}
                    helperText={formErrors.password}
                    value={form.password}
                    onChange={this.setField}
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                  >
                                    Registrar
                  </Button>
                  <Grid container>
                    <Grid item>
                      <Link to="/sign_in" variant="body2">
                                            ¿Ya tienes una cuenta? Identificate
                      </Link>
                    </Grid>
                  </Grid>
                </form>
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  <Grid item>
                    {error ? (
                      <Alert severity="error">
                        <AlertTitle>Error</AlertTitle>
                        {error}
                      </Alert>
                    ) : null}
                  </Grid>
                </Grid>
              </div>
            </Container>
          )}
        </>
      );
    }
}
