import React from 'react';
import {Switch} from 'react-router-dom';
import DashBoard from '../pages/Dashboard';
import Route from './Route';
import PublicLayout from '../pages/layouts/PublicLayout';
import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import PrivateLayout from '../pages/layouts/PrivateLayout';
import Group from '../pages/Group';

export default function SwitchRoutes() {
    return (
        <Switch>
            <Route path="/user_group/:id" component={Group} layout={PrivateLayout} isPrivate/>
            <Route path="/sign_in" component={SignIn} layout={PublicLayout} onlyGuest/>
            <Route path="/sign_up" component={SignUp} layout={PublicLayout} onlyGuest/>
            <Route path="/" component={DashBoard} layout={PrivateLayout} isPrivate/>
        </Switch>
    );
}
