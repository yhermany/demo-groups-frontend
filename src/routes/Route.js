import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import PublicLayout from '../pages/layouts/PublicLayout';

const RouteWrapper = ({
  component: Component, layout: Layout, isPrivate, onlyGuest, token, user, ...otherProps
}) => {
  const signed = !!token;

  if (isPrivate && !signed) {
    return <Redirect to="/sign_in" />;
  }

  if (onlyGuest && signed) {
    return <Redirect to="/" />;
  }

  return (
    <Route
      {...otherProps}
      render={(props) => (
        <>
          {Layout ? (
            <Layout {...props}>
              <Component {...props} />
            </Layout>
          )
            : <Component {...props} />}
        </>
      )}
    />
  );
};

RouteWrapper.propTypes = {
  isPrivate: PropTypes.bool,
  component: PropTypes.elementType.isRequired,
  layout: PropTypes.elementType,
  token: PropTypes.string,
  user: PropTypes.object,
  onlyGuest: PropTypes.bool,
};
RouteWrapper.defaultProps = {
  isPrivate: false,
  layout: PublicLayout,
};

const mapStateToProps = (state) => ({
  token: state.auth.token,
  user: state.auth.user,
});

const mapDispatchToProps = () => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RouteWrapper);
