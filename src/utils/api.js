// export const base = '';
export const base = 'https://demo-groups-api.herokuapp.com';

export const OK = 'ok';
export const RESPONSE_NOT_FOUND = 'not_found';
export const RESPONSE_BAD_REQUEST = 'bad_request';
export const RESPONSE_UNAUTHORIZED = 'unauthorized';
export const UNKNOWN_ERROR = 'Ocurrio algo inesperado, vuelva a intentarlo porfavor.';

export const SIGN_UP = `${base}/api/v1/sessions/sign_up`;
export const SIGN_IN = `${base}/api/v1/sessions/sign_in`;

export const GET_USER_GROUPS = `${base}/api/v1/user_groups`;
export const GET_USERS = `${base}/api/v1/users`;
export const CREATE_USER_GROUPS = `${base}/api/v1/user_groups/create`;
export const CREATE_NOTE = `${base}/api/v1/user_groups/:GROUP_ID/notes`;
export const ADD_USER_GROUP = `${base}/api/v1/user_groups/:GROUP_ID/add_user`;
export const ACTIVATE_USER_GROUP = `${base}/api/v1/user_groups/:GROUP_ID/activate`;


export const SIGN_OUT = `${base}/api/v1/sessions/sign_out`;
