import AuthActionCreator from '../actions/auth';

let store;

function setTopLevelStore(storeRef) {
  store = storeRef;
}

function dispatch(action) {
  if (store) {
    store.dispatch(action);
  } else {
    console.log('store no fue declarado.');
  }
}

function logout() {
  store.dispatch(AuthActionCreator.signOut());
}

function getStore() {
  return store;
}

const StoreService = {
  setTopLevelStore,
  dispatch,
  logout,
  getStore,
};
export default StoreService;
