import { SIGN_IN, SIGN_OUT } from './types';
import EntityActionCreator from './entity';

const signIn = (user, token) => ({
  type: SIGN_IN,
  payload: {
    user,
    token,
  },
});

const signOut = () => (dispatch) => {
  dispatch(EntityActionCreator.reset());
  dispatch(removeUser());
};

const removeUser = () => ({ type: SIGN_OUT, payload: {} });

const AuthActionCreator = {
  signIn,
  signOut,
  removeUser,
};

export default AuthActionCreator;
