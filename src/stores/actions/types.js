export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';

export const RESET_ENTITY = 'RESET_ENTITY';
export const SET_USER_GROUPS = 'SET_USER_GROUPS';
export const SET_USER_GROUP = 'SET_USER_GROUP';
