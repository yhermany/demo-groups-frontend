import {
  RESET_ENTITY, SET_USER_GROUP, SET_USER_GROUPS,
} from './types';
// eslint-disable-next-line import/no-cycle
import EntityApiFacade from '../../core/facades/EntityApiFacade';

const setUserGroups = (userGroups) => ({
  type: SET_USER_GROUPS,
  payload: {
    userGroups,
  },
});

const getUserGroups = () => async (dispatch, getState) => {
  const store = getState();
  const apiFacade = new EntityApiFacade(store.auth.token);
  await apiFacade.getUserGroups((userGroups) => {
    dispatch(setUserGroups(userGroups));
  });
};

const setUserGroup = (userGroup, notes) => ({
  type: SET_USER_GROUP,
  payload: {
    userGroup,
    notes,
  },
});

const getUserGroup = (id) => async (dispatch, getState) => {
  const store = getState();
  const apiFacade = new EntityApiFacade(store.auth.token);
  await apiFacade.getUserGroup(id, ({ user_group, notes }) => {
    dispatch(setUserGroup(user_group, notes));
  });
};


const reset = () => ({
  type: RESET_ENTITY,
  payload: {},
});

const EntityActionCreator = {
  reset,
  getUserGroups,
  setUserGroups,
  getUserGroup,
  setUserGroup,
};

export default EntityActionCreator;
