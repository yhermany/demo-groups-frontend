import createEncryptor from 'redux-persist-transform-encrypt';

const encryptor = createEncryptor({
  secretKey: 'demo-v1',
  onError(error) {
    console.log('error on load local data.', error.message);
  },
});

export default encryptor;
