import { SIGN_OUT, SIGN_IN } from '../actions/types';

const initialState = () => ({
  user: null,
  token: null,
});

const AuthReducer = (state = initialState(), action) => {
  switch (action.type) {
    case SIGN_IN:
      return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
      };
    case SIGN_OUT:
      return initialState();
    default:
      return state;
  }
};
export default AuthReducer;
