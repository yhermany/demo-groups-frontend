import {
  RESET_ENTITY,
  SET_USER_GROUP,
  SET_USER_GROUPS,
} from '../actions/types';

const initialState = () => ({
  userGroups: [],
  userGroup: null,
  notes: [],
});

const EntityReducer = (state = initialState(), action) => {
  switch (action.type) {
    case SET_USER_GROUPS:
      return {
        ...state,
        userGroups: action.payload.userGroups,
      };
    case SET_USER_GROUP:
      return {
        ...state,
        userGroup: action.payload.userGroup,
        notes: action.payload.notes,
      };
    case RESET_ENTITY:
      return initialState();
    default:
      return state;
  }
};
export default EntityReducer;
