import { combineReducers } from 'redux';
import AuthReducer from './auth';
import EntityReducer from './entity';

const reducer = combineReducers({
  auth: AuthReducer,
  entity: EntityReducer,
});

export default reducer;
