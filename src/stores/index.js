import { applyMiddleware, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import ReduxThunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';
import encryptor from './encryptor';
import reducer from './reducers';

const persistConfig = {
  key: 'fms_mobile',
  storage,
  stateReconciler: autoMergeLevel2,
  transforms: [encryptor],
};

const persistorReducer = persistReducer(persistConfig, reducer);

const middlewares = [ReduxThunk];

// if (process.env.NODE_ENV === 'development') {
//   // eslint-disable-next-line global-require
//   const { logger } = require('redux-logger');
//   middlewares.push(logger);
// }

const store = createStore(persistorReducer, applyMiddleware(...middlewares));

const persistor = persistStore(store);

const StoreConfig = {
  store,
  persistor,
};
export default StoreConfig;
