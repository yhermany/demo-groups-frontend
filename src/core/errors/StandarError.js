export default class StandarError extends Error {
  constructor(message) {
    super(message);
    this.name = 'StandarError';
  }
}
