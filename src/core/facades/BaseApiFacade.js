import EntityQuery from '../api/EntityQuery';
import Client from '../api/Client';

export default class BaseApiFacade {
  constructor() {
    const client = Client.getInstance();
    this.query = new EntityQuery(client);
  }

  useToken = (token) => {
    const client = Client.getInstance(token);
    this.query = new EntityQuery(client);
  };
}
