import EntityQuery from '../api/EntityQuery';
import Client from '../api/Client';
import BaseApiFacade from './BaseApiFacade';
// eslint-disable-next-line import/no-cycle
import handleRequest from '../api/HandleRequest';

export default class EntityApiFacade extends BaseApiFacade {
  constructor(token) {
    super();
    const client = Client.getInstance(token);
    this.query = new EntityQuery(client);
  }

  getUserGroups = async (success = null, fail = null) => {
    await handleRequest(async () => this.query.getUserGroups(), success, fail);
  };

  createUserGroup = async (data, success = null, fail = null) => {
    await handleRequest(async () => this.query.createUserGroup(data), success, fail);
  };

  getUserGroup = async (id, success = null, fail = null) => {
    await handleRequest(async () => this.query.getUserGroup(id), success, fail);
  };

  createNote = async (data, success = null, fail = null) => {
    await handleRequest(async () => this.query.createNote(data), success, fail);
  };

  getUsers = async (success = null, fail = null) => {
    await handleRequest(async () => this.query.getUsers(), success, fail);
  };

  addUserGroup = async (data, success = null, fail = null) => {
    await handleRequest(async () => this.query.addUserGroup(data), success, fail);
  };

  activateUserGroup = async (data, success = null, fail = null) => {
    await handleRequest(async () => this.query.activateUserGroup(data), success, fail);
  };
}
