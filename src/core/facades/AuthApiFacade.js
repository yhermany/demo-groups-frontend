import AuthQuery from '../api/AuthQuery';
import Client from '../api/Client';
import BaseApiFacade from './BaseApiFacade';
import handleRequest from '../api/HandleRequest';

export default class AuthApiFacade extends BaseApiFacade {
  constructor() {
    super();
    const client = Client.getInstance();
    this.query = new AuthQuery(client);
  }

  signUp = async (data, success = null, fail = null) => {
    await handleRequest(async () => this.query.signUp(data), success, fail);
  };

  signIn = async (data, success = null, fail = null) => {
    await handleRequest(async () => this.query.signIn(data), success, fail);
  };
}
