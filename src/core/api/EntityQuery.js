import {
  CREATE_NOTE,
  CREATE_USER_GROUPS,
  GET_USER_GROUPS, GET_USERS,
  ADD_USER_GROUP,
  ACTIVATE_USER_GROUP,
} from '../../utils/api';
import BaseQuery from './BaseQuery';

export default class EntityQuery extends BaseQuery {
    getUserGroups = async () => this.callQuery(async () => this.apiClient.get(GET_USER_GROUPS));

    getUserGroup = async (id) => this.callQuery(async () => this.apiClient.get(`${GET_USER_GROUPS}/${id}`));

    getUsers = async () => this.callQuery(async () => this.apiClient.post(GET_USERS));

    createUserGroup = async (data) => this.callQuery(
      async () => this.apiClient.post(CREATE_USER_GROUPS, data),
    );

    createNote = async ({ groupId, note }) => this.callQuery(
      async () => this.apiClient.post(CREATE_NOTE.replace(':GROUP_ID', groupId), note),
    );

    addUserGroup = async ({ groupId, form }) => this.callQuery(
      async () => this.apiClient.post(ADD_USER_GROUP.replace(':GROUP_ID', groupId), form),
    );

    activateUserGroup = async ({ groupId }) => this.callQuery(
      async () => this.apiClient.get(ACTIVATE_USER_GROUP.replace(':GROUP_ID', groupId)),
    );
}
