import StandarError from '../errors/StandarError';
import { OK, UNKNOWN_ERROR, RESPONSE_UNAUTHORIZED } from '../../utils/api';
import UnauthorizedError from '../errors/UnauthorizedError';

export default class BaseQuery {
  constructor(apiClient) {
    this.apiClient = apiClient;
  }

  callQuery = async (query) => {
    let response;
    try {
      response = await query();
    } catch (e) {
      throw new StandarError(UNKNOWN_ERROR);
    }
    if (response.data.status === OK) {
      return response.data.data;
    } if (response.data.status === RESPONSE_UNAUTHORIZED) {
      throw new UnauthorizedError(response.data.data.message);
    } else {
      throw new StandarError(response.data.data.message);
    }
  };
}
