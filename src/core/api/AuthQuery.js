import {
  SIGN_IN,
  SIGN_OUT,
  SIGN_UP,
} from '../../utils/api';
import BaseQuery from './BaseQuery';

export default class AuthQuery extends BaseQuery {
    signUp = async (data) => this.callQuery(async () => this.apiClient.post(SIGN_UP, data));

    signIn = async (data) => this.callQuery(async () => this.apiClient.post(SIGN_IN, data));


    signOut = async (fcm_token) => await this.callQuery(async () => this.apiClient.delete(SIGN_OUT, {
      token: fcm_token,
    }));
}
