import StoreService from '../../stores/utils/store';

const handleRequest = async (request, success = null, fail = null) => {
  try {
    const response = await request();
    if (success) {
      success(response);
    }
  } catch (e) {
    if (e.name === 'UnauthorizedError') {
      StoreService.logout();
    } else if (fail) {
      fail(e);
    }
  }
};

export default handleRequest;
