import axios from 'axios';

export default class Client {
  static getHeaders() {
    return {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };
  }

  constructor() {
    Client.getHeaders = Client.getHeaders.bind(this);
    Client.getInstance = Client.getInstance.bind(this);
  }

  static getInstance(token = null) {
    const headers = Client.getHeaders();
    if (token) {
      headers.Authorization = token;
    }
    return axios.create({
      headers,
    });
  }
}
