import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import CssBaseline from '@material-ui/core/CssBaseline';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import SwitchRoutes from './routes';
import StoreService from './stores/utils/store';
import StoreConfig from './stores';
import ActivityIndicatorWithMessage from './components/ActivityIndicatorWithMessage';
import { BrowserRouter as Router} from "react-router-dom";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#091D4A',
        },
    },
});
export default function App() {
    StoreService.setTopLevelStore(StoreConfig.store);

    return (
        <Router basename={process.env.PUBLIC_URL}>
            <Provider store={StoreConfig.store}>
                <PersistGate
                    loading={
                        <ActivityIndicatorWithMessage message="Iniciando la aplicación"/>
                    }
                    persistor={StoreConfig.persistor}
                >
                        <MuiThemeProvider theme={theme}>
                            <CssBaseline/>
                            <SwitchRoutes/>
                        </MuiThemeProvider>
                </PersistGate>
            </Provider>
        </Router>
    );
}
